#ifndef SWFMASTERWINDOW_H
#define SWFMASTERWINDOW_H

#include <QMainWindow>
#include <QProgressDialog>
#include "swf/SWFDecoder.h"

class AS3SWF;
class SWF;
class SWFDecoder;
class SWFTreeModel;
class EditorContainer;

namespace Ui {
class SWFMasterWindow;
}

class SWFMasterWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit SWFMasterWindow(QWidget *parent = 0);
    ~SWFMasterWindow();

	void timerEvent(QTimerEvent *) override;
private slots:
	void onOpen();
	void onAbout();
	void onSaveAs();
	void onReplaceTag();
	void onItemTreeContextMenuRequested(const QPoint& pos);
	void onItemTreeSelectItem(const QModelIndex& index);
private:
	Ui::SWFMasterWindow *ui;
	AS3SWF* _as3swf;
    SWFTreeModel* _swfObjectModel;
	EditorContainer* _editorContainer;
};

class DecodeSWFProgressDialog
	: public QProgressDialog
{
public:
	DecodeSWFProgressDialog(const QString& name);
	~DecodeSWFProgressDialog();
	int exec() override;
private:
	void timerEvent(QTimerEvent *) override;
	void decode(const QString& name);
private:
	SWF* _swf;
	SWFDecoder _decoder;
};
#endif // SWFMASTERWINDOW_H
