#ifndef ImageHelper_h__
#define ImageHelper_h__

#include <QImage>

class TagDefineBits;
class TagDefineBitsJPEG2;
class TagDefineBitsJPEG3;
class TagDefineBitsLossless;
class TagDefineBitsLossless2;

class ImageHelper
{
public:
	static TagDefineBits* createDefineBits(QImage* image, bool highQuality);
	static TagDefineBitsJPEG2* createDefineBitsJPEG2(QImage* image);
	static TagDefineBitsJPEG3* createDefineBitsJPEG3(QImage* image);
	static TagDefineBitsLossless* createTagDefineBitsLossless(QImage* image);
	static TagDefineBitsLossless2* createTagDefineBitsLossless2(QImage* image);
	static QImage* convertDefineBitsToImage(TagDefineBits* tag);
};
#endif // ImageHelper_h__
