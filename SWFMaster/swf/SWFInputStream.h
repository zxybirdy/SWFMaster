#ifndef SWFInputStream_h__
#define SWFInputStream_h__

#include <string>
#include "SWFTypes.h"
#include "SWFStructs.h"
#include "ByteArray.h"

class SWF;

class SWFInputStream
{
public:
	SWFInputStream(char* data, size_t length);
	SWFInputStream();
	~SWFInputStream();

	Rect	readRect();
	UI8		readUI8();
	UI16	readUI16();
	UI32	readUI32();

	SI8		readSI8();
	SI16	readSI16();

	UI8		readU8();
	UI30	readU30();
	UI32	readU32();
	SI24	readS24();
	SI32	readS32();

	UB		readUB(unsigned int size);
	SB		readSB(unsigned int size);
	FB		readFB(unsigned int size);
	Fixed8	readFixed8();
	Fixed	readFixed();

	FLOAT	readFloat();
	DOUBLE	readDouble();
	void	readBytes(char* dest, int len);
	void	readByteArray(ByteArray* barray, size_t length);

	ColorRGB	readColorRGB();
	ColorRGBA	readColorRGBA();

	Matrix	readMatrix();
	CXFORM*	readColorTransform();
	CXFORMWITHALPHA* readColorTransformWithAlpha();

	std::string readString();
	std::string readString(UI8 len);
	std::string readUTFString();

	void alignBits();
	void setData(char* data, size_t len);

	inline int getLength() {  return _length; }
	inline int getPosition() {  return _position; }
	inline void setPosition(size_t pos) { _position = pos; }
	inline int getRemain() { return _length - _position; }
	inline void skip(size_t size) { _position += size; }
private:
	char*	_data;
	size_t	_length;
	size_t	_position;
	size_t	_bitPosition;
	char	_bitTemp;
};

#endif // SWFReader_h__
