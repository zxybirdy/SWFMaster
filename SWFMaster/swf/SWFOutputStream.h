#ifndef SWFOutputStream_h__
#define SWFOutputStream_h__

#include "SWFTypes.h"
#include "SWFStructs.h"
#include <string>
#include "ByteArray.h"

class SWFOutputStream
{
private:
	size_t _position;
	UI8 bitPos;
	char currentByte;
	ByteArray _byteArray;
private:
	void checkSize(size_t v);
	void writeBits(int, UI32 size);
	void writeByte(char data);
public:
	SWFOutputStream();
	~SWFOutputStream();
	inline char* getData() const { return _byteArray.getData(); }
	inline int getPosition() const { return _position; }
	inline void setPosition(int v) { _position = v; }
	ByteArray* getByteArray();

	void alignBits();
	void reset();

	void writeUI8(UI8);
	void writeUI16(UI16);
	void writeUI32(UI32);

	void writeU8(UI8);
	void writeU30(UI30);
	void writeU32(UI32);

	void writeSI8(SI8);
	void writeSI16(SI16);

	void writeS24(SI24);
	void writeS32(SI32);

	void writeUB(UB, UI32);
	void writeSB(SB, UI32);
	void writeFB(FB);

	void writeFixed(Fixed);
	void writeFixed8(Fixed8);
	void writeFloat(FLOAT);
	void writeDouble(DOUBLE);
	void writeBytes(const char*, size_t);
	void writeByteArray(ByteArray* ba);

	void writeString(std::string);
	void writeUTFString(std::string);

	void writeRect(const Rect& rect);
	void writeColorRGB(const ColorRGB& color);
	void writeColorRGBA(const ColorRGBA& color);
	void writeMatrix(const Matrix& matrix);
	void writeColorTransformWithAlpha(const CXFORMWITHALPHA* transform);
};

#endif // SWFOutputStream_h__
