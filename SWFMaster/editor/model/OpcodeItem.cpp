#include "OpcodeItem.h"
#include "swf/SWFInputStream.h"
#include "swf/abc/Opcode.h"

OpcodeItem::OpcodeItem( OpcodeInstruction* inst, CpoolInfo* ci)
	: _instruction(inst)
	, _pool(ci)
	, prev(nullptr)
	, next(nullptr)
{
	init();
}

void OpcodeItem::init()
{
	QString text;
	SWFInputStream input(_instruction->data, _instruction->dataLen);
	for (size_t i = 0; i < _instruction->dataLen; ++i)
	{
		char n = _instruction->data[i];
		QString ns = QString::number(n & 0xff, 16);
		if (ns.size() == 1) ns = "0" + ns;
		ns = ns + " ";
		text += ns.toUpper();
	}

	setData(text, Qt::DisplayRole, 1);
	setData(_instruction->operation->name.c_str(), Qt::DisplayRole, 2);
	setData(_instruction->paramString.c_str(), Qt::DisplayRole, 3);
}

void OpcodeItem::setOffset( size_t offset )
{
	setData(offset, Qt::DisplayRole, 0);
}

